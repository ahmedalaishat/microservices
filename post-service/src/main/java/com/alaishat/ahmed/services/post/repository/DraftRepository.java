package com.alaishat.ahmed.services.post.repository;

import com.alaishat.ahmed.services.post.model.Draft;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository

public class DraftRepository {

    private List<Draft> drafts = new ArrayList<>();

    public Draft add(Draft draft) {
        draft.setId((drafts.size() + 1));
        drafts.add(draft);
        return draft;
    }

    public List<Draft> find(List<Integer> ids) {
        return drafts.stream().filter(p -> ids.contains(p.getId())).collect(Collectors.toList());
    }

    public Draft findDraftById(Long id) {

        Optional<Draft> postDraft = drafts.stream().filter(p -> p.getId() == id).findFirst();
        if (postDraft.isPresent())
            return postDraft.get();
        else
            return null;
    }
}
