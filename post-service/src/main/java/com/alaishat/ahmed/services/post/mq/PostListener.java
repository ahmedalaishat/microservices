package com.alaishat.ahmed.services.post.mq;

import com.alaishat.ahmed.services.post.PostApplication;
import com.alaishat.ahmed.services.post.model.Post;
import com.alaishat.ahmed.services.post.repository.PostRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Ahmed Al-Aishat on 5/31/2021.
 * sample-spring-cloud-comm Project.
 */
@Service
public class PostListener {

    private static final Logger log = LoggerFactory.getLogger(PostListener.class);

    @Autowired
    PostRepository repository;

    @RabbitListener(queues = PostApplication.QUEUE_SPECIFIC_NAME)
    public void receivePost(final Post post) {
        log.info("Received message and deserialized to 'Post': {}", post.toString());
        repository.add(post);
    }
}
