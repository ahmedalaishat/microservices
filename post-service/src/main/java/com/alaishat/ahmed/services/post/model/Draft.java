package com.alaishat.ahmed.services.post.model;

public class Draft {
    private int id;
    private String newName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Draft(int id, String newName) {
        this.id = id;
        this.newName = newName;
    }

    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }
}
