package com.alaishat.ahmed.services.post.model;

/**
 * Created by Ahmed Al-Aishat on 6/17/2021.
 * sample-spring-cloud-comm Project.
 */
public class UserWithAddress {
    public String username;
    public String address;

    public UserWithAddress() {
    }

    public UserWithAddress(String username, String address) {
        this.username = username;
        this.address = address;
    }
}
