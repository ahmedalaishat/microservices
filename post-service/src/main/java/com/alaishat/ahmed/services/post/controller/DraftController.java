package com.alaishat.ahmed.services.post.controller;


import com.alaishat.ahmed.services.post.model.Draft;
import com.alaishat.ahmed.services.post.repository.DraftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DraftController {

    @Autowired
    DraftRepository Drepository;


    @PostMapping("/draft")
    public Draft put(@RequestBody Draft draft) {
        return Drepository.add(draft);
    }

    @GetMapping("/draft/{id}")
    public Draft findDraftById(@PathVariable("id") Long id) {
        return Drepository.findDraftById(id);
    }

    @GetMapping("/Alldrft/ids")
    public List<Draft> find(@RequestBody List<Integer> ids) {
        return Drepository.find(ids);
    }


}



