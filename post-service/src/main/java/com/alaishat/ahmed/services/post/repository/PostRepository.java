package com.alaishat.ahmed.services.post.repository;

import com.alaishat.ahmed.services.post.model.Post;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class PostRepository {
    private List<Post> posts = new ArrayList<>();

    public Post add(Post post) {
        post.id = posts.size() + 1;
        posts.add(post);
        return post;
    }

    public Post update(Post post) {
        posts.set(post.id, post);
        return post;
    }

    public Post findById(int id) {
        Optional<Post> product = posts.stream().filter(p -> p.id == id).findFirst();
        if (product.isPresent())
            return product.get();
        else
            return null;
    }

    public void delete(int id) {
        posts.remove(id);
    }

    public List<Post> find(List<Integer> ids) {
        return posts.stream().filter(p -> ids.contains(p.id)).collect(Collectors.toList());
    }
}
