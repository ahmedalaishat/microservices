package com.alaishat.ahmed.services.post.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Ahmed Al-Aishat on 5/31/2021.
 * sample-spring-cloud-comm Project.
 */
public class Post implements Serializable {
    @JsonProperty("id") public int id;
    @JsonProperty("name") public String name;
    @JsonProperty("content") public String content;
    @JsonProperty("username") public String username;
    @JsonProperty("userAddress") public String userAddress;

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", content='" + content + '\'' +
                ", username='" + username + '\'' +
                ", userAddress='" + userAddress + '\'' +
                '}';
    }
}
