package com.alaishat.ahmed.services.post.controller;

import com.alaishat.ahmed.services.post.model.Post;
import com.alaishat.ahmed.services.post.model.UserWithAddress;
import com.alaishat.ahmed.services.post.repository.PostRepository;
import com.alaishat.ahmed.services.post.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PostController {

    @Autowired
    PostRepository repository;

    @Autowired
    UserService userService;

    @PostMapping("/post")
    public Post add(@RequestBody Post post, @RequestParam long userId) {
        UserWithAddress userWithAddress = userService.getUserWithAddressById(userId);
        post.username = userWithAddress.username;
        post.userAddress = userWithAddress.address;
        return repository.add(post);
    }


    /// this is our implementation
    @PutMapping
    public Post update(@RequestBody Post post) {
        return repository.update(post);
    }

    @GetMapping("/post/{id}")
    public Post findById(@PathVariable("id") int id) {
        return repository.findById(id);
    }


    @PostMapping("post/ids")
    public List<Post> find(@RequestBody List<Integer> ids) {
        return repository.find(ids);
    }

    @DeleteMapping("post/{id}")
    public void delete(@PathVariable("id") int id) {
        repository.delete(id);
    }

}
