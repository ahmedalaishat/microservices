package com.alaishat.ahmed.services.post.service;

import com.alaishat.ahmed.services.post.model.UserWithAddress;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Ahmed Al-Aishat on 6/14/2021.
 * sample-spring-cloud-comm Project.
 */
@Service
public class UserService {

    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "defaultUsername")
    public UserWithAddress getUserWithAddressById(long userId) {
        UserWithAddress userWithAddress = restTemplate.getForObject("http://user-service/user-with-address/{id}",
                UserWithAddress.class, userId);
        return userWithAddress;
    }

    private UserWithAddress defaultUsername(long userId) {
        return new UserWithAddress("User1", "Homs");
    }

}
