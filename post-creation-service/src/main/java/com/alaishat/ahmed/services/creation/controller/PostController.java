package com.alaishat.ahmed.services.creation.controller;

import com.alaishat.ahmed.services.creation.model.Post;
import com.alaishat.ahmed.services.creation.mq.PostSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Ahmed Al-Aishat on 5/31/2021.
 * sample-spring-cloud-comm Project.
 */
@RestController
public class PostController {

    @Autowired
    PostSender rabbitMQPostSender;

    @PostMapping
    public String create(@RequestBody Post post) {
//        Post post = new Post();
//        post.id = 1;
//        post.name = "name";
//        post.content = "nameame";
        rabbitMQPostSender.sendMessage(post);
        return "Message sent to the RabbitMQ JavaInUse Successfully";
    }

}
