package com.alaishat.ahmed.services.creation.mq;

import com.alaishat.ahmed.services.creation.PostCreationApplication;
import com.alaishat.ahmed.services.creation.model.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Ahmed Al-Aishat on 5/31/2021.
 * sample-spring-cloud-comm Project.
 */
@Service
public class PostSender {
    private static final Logger log = LoggerFactory.getLogger(PostSender.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(Post post) {
        log.info("Sending message...");
        rabbitTemplate.convertAndSend(PostCreationApplication.EXCHANGE_NAME, PostCreationApplication.ROUTING_KEY, post);
    }
}
