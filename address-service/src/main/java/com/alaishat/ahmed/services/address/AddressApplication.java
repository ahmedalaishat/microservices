package com.alaishat.ahmed.services.address;

import com.alaishat.ahmed.services.address.model.Address;
import com.alaishat.ahmed.services.address.repository.AddressRepository;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.cloud.sleuth.Sampler;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
@RibbonClients({
        @RibbonClient(name = "address-service"),
        @RibbonClient(name = "post-service"),
        @RibbonClient(name = "user-service"),
})
public class AddressApplication {

    @LoadBalanced
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    public static void main(String[] args) {
        new SpringApplicationBuilder(AddressApplication.class).web(true).run(args);
    }

    @Bean
    AddressRepository repository() {
        AddressRepository repository = new AddressRepository();
        repository.add(new Address("Homs"));
        repository.add(new Address("Daraa"));
        repository.add(new Address("Damascus"));
        repository.add(new Address("Aleppo"));
        return repository;
    }

    @Bean
    public Sampler defaultSampler(){
        return new AlwaysSampler();
    }
}
