package com.alaishat.ahmed.services.address.model;

/**
 * Created by Ahmed Al-Aishat on 6/17/2021.
 * sample-spring-cloud-comm Project.
 */
public class Address {

    private Long id;
    private String city;

    public Address() {

    }

    public Address(String city) {
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
