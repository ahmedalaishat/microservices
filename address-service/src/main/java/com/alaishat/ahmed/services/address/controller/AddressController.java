package com.alaishat.ahmed.services.address.controller;

import com.alaishat.ahmed.services.address.model.Address;
import com.alaishat.ahmed.services.address.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AddressController {

    @Autowired
    AddressRepository repository;

    @PostMapping
    public Address add(@RequestBody Address user) {
        return repository.add(user);
    }

    @PutMapping
    public Address update(@RequestBody Address user) {
        return repository.update(user);
    }

    /// this is our implementation
    @GetMapping("/{id}")
    public Address findById(@PathVariable("id") Long id) {
        return repository.findById(id);
    }

    @PostMapping("/ids")
    public List<Address> find(@RequestBody List<Long> ids) {
        return repository.find(ids);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        repository.delete(id);
    }

}
