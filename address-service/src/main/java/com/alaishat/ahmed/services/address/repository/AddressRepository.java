package com.alaishat.ahmed.services.address.repository;

import com.alaishat.ahmed.services.address.model.Address;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class AddressRepository {

    private List<Address> users = new ArrayList<>();

    public Address add(Address user) {
        user.setId((long) (users.size() + 1));
        users.add(user);
        return user;
    }

    public Address update(Address user) {
        users.set(user.getId().intValue() - 1, user);
        return user;
    }

    public Address findById(Long id) {
        Optional<Address> user = users.stream().filter(p -> p.getId().equals(id)).findFirst();
        return user.orElse(null);
    }

    public void delete(Long id) {
        users.remove(id.intValue());
    }

    public List<Address> find(List<Long> ids) {
        return users.stream().filter(p -> ids.contains(p.getId())).collect(Collectors.toList());
    }
}
