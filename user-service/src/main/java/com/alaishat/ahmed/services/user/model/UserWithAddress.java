package com.alaishat.ahmed.services.user.model;

/**
 * Created by Ahmed Al-Aishat on 6/17/2021.
 * sample-spring-cloud-comm Project.
 */
public class UserWithAddress {
    public String username;
    public String address;
}
