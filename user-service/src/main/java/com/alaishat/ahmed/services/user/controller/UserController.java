package com.alaishat.ahmed.services.user.controller;

import com.alaishat.ahmed.services.user.model.User;
import com.alaishat.ahmed.services.user.model.UserWithAddress;
import com.alaishat.ahmed.services.user.repository.UserRepository;
import com.alaishat.ahmed.services.user.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    UserRepository repository;

    @Autowired
    AddressService addressService;

    @PostMapping
    public User add(@RequestBody User user) {
        return repository.add(user);
    }

    @PutMapping
    public User update(@RequestBody User user) {
        return repository.update(user);
    }

    /// this is our implementation
    @GetMapping("/{id}")
    public User findById(@PathVariable("id") Long id) {
        return repository.findById(id);
    }

    @GetMapping("/user-with-address/{id}")
    public UserWithAddress findWithAddressById(@PathVariable("id") Long id) {
        User user = repository.findById(id);
        UserWithAddress userWithAddress = new UserWithAddress();
        userWithAddress.username = user.getName();
        userWithAddress.address = addressService.getAddressNameById(user.getAddressId());
        return userWithAddress;
    }

    @PostMapping("/ids")
    public List<User> find(@RequestBody List<Long> ids) {
        return repository.find(ids);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        repository.delete(id);
    }

}
