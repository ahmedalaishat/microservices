package com.alaishat.ahmed.services.user.repository;

import com.alaishat.ahmed.services.user.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class UserRepository {

	private List<User> users = new ArrayList<>();
	
	public User add(User user) {
		user.setId((long) (users.size()+1));
		users.add(user);
		return user;
	}
	
	public User update(User user) {
		users.set(user.getId().intValue() - 1, user);
		return user;
	}
	
	public User findById(Long id) {
		Optional<User> product = users.stream().filter(p -> p.getId().equals(id)).findFirst();
		if (product.isPresent())
			return product.get();
		else
			return null;
	}
	
	public void delete(Long id) {
		users.remove(id.intValue());
	}
	
	public List<User> find(List<Long> ids) {
		return users.stream().filter(p -> ids.contains(p.getId())).collect(Collectors.toList());
	}
}
