package com.alaishat.ahmed.services.user.model;

public class User {

    private Long id;
    private String name;
    private Long addressId;

    public User() {

    }

    public User(String name) {
        this.name = name;
    }

    public User(String name, Long addressId) {
        this.name = name;
        this.addressId = addressId;
    }

    public Long getId() {
        return id;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
