package com.alaishat.ahmed.services.user;

import com.alaishat.ahmed.services.user.model.User;
import com.alaishat.ahmed.services.user.repository.UserRepository;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.cloud.sleuth.Sampler;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
@RibbonClients({
        @RibbonClient(name = "address-service"),
        @RibbonClient(name = "post-service"),
})
public class UserApplication {

    @LoadBalanced
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    public static void main(String[] args) {
        new SpringApplicationBuilder(UserApplication.class).web(true).run(args);
    }

    @Bean
    UserRepository repository() {
        UserRepository repository = new UserRepository();
        repository.add(new User("User1", 1L));
        repository.add(new User("User2", 2L));
        repository.add(new User("User3", 3L));
        repository.add(new User("User4", 4L));
        repository.add(new User("User5", 4L));
        repository.add(new User("User6", 3L));
        repository.add(new User("User7", 4L));
        repository.add(new User("User8", 2L));
        repository.add(new User("User9", 1L));
        repository.add(new User("User10", 2L));
        return repository;
    }

    @Bean
    public Sampler defaultSampler(){
        return new AlwaysSampler();
    }
}
