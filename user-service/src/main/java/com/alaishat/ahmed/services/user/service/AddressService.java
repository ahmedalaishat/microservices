package com.alaishat.ahmed.services.user.service;

import com.alaishat.ahmed.services.user.model.Address;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AddressService {

    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "defaultAddress")
    public String getAddressNameById(long id) {
        Address address = restTemplate.getForObject("http://address-service/{id}", Address.class, id);
        return address.getCity();
    }

    private String defaultAddress(long id) {
        return "Homs";
    }

}


